import 'react-native-gesture-handler';
import React, {useEffect} from 'react';
import RootStackScreen from './js/screen/RootStack';
import {NavigationContainer} from '@react-navigation/native';
import {Provider, useDispatch} from 'react-redux';
import store from './js/store';
import {StatusBar} from 'react-native';

const App = ({navigation}) => {
  // const dispatch = useDispatch();
  useEffect(() => {
    // dispatch(getPopularAction());
  }, []);

  return (
    <Provider store={store}>
      <NavigationContainer>
        <StatusBar hidden={false} />
        <RootStackScreen />
      </NavigationContainer>
    </Provider>
  );
};

export default App;

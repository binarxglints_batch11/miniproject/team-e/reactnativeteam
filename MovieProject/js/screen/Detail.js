import React, {useEffect, useState} from 'react';
import {
  Text,
  View,
  Image,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  FlatList,
  Modal,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {YOUTUBE_URL, IMAGE_URL, YOUTUBE_API_KEY} from '../constant/general';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {color} from '../styles/default';
import LinearGradient from 'react-native-linear-gradient';
import YouTube from 'react-native-youtube';
import {
  getMovieAction,
  getMovieReviewAction,
  getVideosAction,
} from '../actions/movieAction';

const Detail = props => {
  const dispatch = useDispatch();
  const [play, setPlay] = useState(false);

  const selectedMovie = useSelector(state => state.movies.movie);
  const reviews = useSelector(state => state.movies.reviews);
  const videos = useSelector(state => state.movies.videos);
  const loading = useSelector(state => state.movies.loading);

  useEffect(() => {
    dispatch(getMovieAction(props.route.params?.movieId));
    dispatch(getMovieReviewAction(props.route.params?.movieId));
    dispatch(getVideosAction(props.route.params?.movieId));
  }, [props.route.params?.movieId, play]);

  const renderGenre = genres => {
    return genres?.map((genre, i) => (
      <View
        key={i}
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text key={i} style={{color: color.white, fontSize: 10}}>
          {genre.name}
        </Text>
        {i != genres.length - 1 && (
          <View
            style={{
              backgroundColor: color.red,
              marginHorizontal: 5,
              width: 5,
              height: 5,
              borderRadius: 5,
              marginHorizontal: 3,
            }}></View>
        )}
      </View>
    ));
  };
  const renderHeaderBar = () => (
    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
      <TouchableOpacity
        onPress={() => props.navigation.navigate('MainTab')}
        style={{
          width: 50,
          height: 50,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <AntDesign name="left" size={25} color={color.white} />
      </TouchableOpacity>
    </View>
  );
  const renderHeaderSection = () => (
    <ImageBackground
      source={{uri: `${IMAGE_URL}/${selectedMovie.backdrop_path}`}}
      resizeMode="cover"
      style={{
        width: '100%',
        height: 450,
      }}>
      <View style={{flex: 1}}>
        {renderHeaderBar()}
        <View style={{flex: 1, justifyContent: 'flex-end'}}>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 0, y: 1}}
            colors={['transparent', '#000']}
            style={{
              width: '100%',
              height: 200,
              alignItems: 'center',
              justifyContent: 'flex-end',
            }}>
            <Text
              style={{
                color: color.white,
                fontSize: 40,
                fontWeight: 'bold',
                textAlign: 'center',
              }}>
              {selectedMovie.title}
            </Text>
            {renderCategoryAndRating()}
          </LinearGradient>
        </View>
      </View>
    </ImageBackground>
  );

  const renderCategoryAndRating = () => (
    <View
      style={{
        marginTop: -15,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          marginVertical: 10,
        }}>
        {renderGenre(selectedMovie?.genres)}
      </View>
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity
          style={styles.categoryContainer}
          onPress={() =>
            props.navigation.navigate('Reviews', {
              reviews,
            })
          }>
          <AntDesign name="message1" color={color.black} size={15} />
          <Text style={{color: color.black, fontSize: 12, marginHorizontal: 5}}>
            {reviews.length}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.categoryContainer}
          onPress={() =>
            props.navigation.navigate('PlayVideo', {
              videos,
            })
          }>
          <AntDesign name="caretright" color={color.black} size={15} />
          <Text
            style={{
              color: color.black,
              fontSize: 12,
              marginHorizontal: 5,
              fontWeight: 'bold',
            }}>
            Play Trailer
          </Text>
        </TouchableOpacity>
        <View style={styles.categoryContainer}>
          <AntDesign name="star" color="yellow" size={15} />
          <Text style={{color: color.black, fontSize: 12, marginHorizontal: 5}}>
            {selectedMovie.vote_average}
          </Text>
        </View>
      </View>
    </View>
  );

  return (
    <>
      {!loading ? (
        <ScrollView
          contentContainerStyle={{
            backgroundColor: color.purple,
            paddingHorizontal: 5,
            paddingBottom: 40,
          }}
          style={{backgroundColor: color.purple}}>
          {renderHeaderSection()}
          <View style={{marginVertical: 10}}>
            <Text
              style={{
                fontSize: 18,
                color: color.white,
                fontWeight: 'bold',
                marginVertical: 10,
              }}>
              Overview
            </Text>
            <Text
              style={{
                fontSize: 14,
                color: color.white,
                textAlign: 'justify',
              }}>
              {selectedMovie.overview}
            </Text>
          </View>
        </ScrollView>
      ) : (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            alignContent: 'center',
            backgroundColor: color.purple,
          }}>
          <ActivityIndicator size="large" color={color.darkRed} />
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  categoryContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: color.white,
    padding: 5,
    borderRadius: 10,
    marginHorizontal: 5,
  },
});

export default Detail;

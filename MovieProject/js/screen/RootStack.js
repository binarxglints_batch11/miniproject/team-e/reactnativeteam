import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';

// Screen
import MainTab from './MainTab';
import Login from './Login';
import Register from './Register';
import SplashScreen from './Splash';
import Detail from './Detail';
import Reviews from './Reviews';
import PlayVideo from './common/PlayVideo';

const RootStack = createStackNavigator();

const RootStackScreen = ({}) => (
  <RootStack.Navigator headerMode="none">
    <RootStack.Screen name="SplashScreen" component={SplashScreen} />
    <RootStack.Screen name="Login" component={Login} />
    <RootStack.Screen name="Register" component={Register} />
    <RootStack.Screen name="MainTab" component={MainTab} />
    <RootStack.Screen name="Detail" component={Detail} />
    <RootStack.Screen name="Reviews" component={Reviews} />
    <RootStack.Screen name="PlayVideo" component={PlayVideo} />
  </RootStack.Navigator>
);

export default RootStackScreen;

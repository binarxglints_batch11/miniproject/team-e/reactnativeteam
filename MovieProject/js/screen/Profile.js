import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {color} from '../styles/default';

const Profile = ({navigation}) => {
  const [userName, setUserName] = useState('');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  useEffect(() => {
    // let userId;
    AsyncStorage.getItem('userId').then(e => {
      axios({
        method: 'GET',
        url: 'https://septian.dev/movie-api/users/' + e,
      })
        .then(({data}) => {
          setUserName(data.data.userName);
          setName(data.data.name);
          console.log('ini e', e);
          console.log('ini data', data);
        })
        .catch(err => {
          setUserName('');
          setPassword('');
          console.log(JSON.stringify(err));
          // alert(err.message);
        });
    });
  }, []);

  const removeToken = () => {
    AsyncStorage.removeItem('access_token', err => {
      console.log('ini err', err);

      if (err == null) {
        AsyncStorage.removeItem('userId', err2 => {
          if (err2 == null) {
            navigation.navigate('Login');
          }
        });
      }
    });
  };

  return (
    <View style={styling.container}>
      <View style={styling.photo}>
        <Image
          style={styling.pic}
          source={{
            uri:
              'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7e/Circle-icons-profile.svg/1024px-Circle-icons-profile.svg.png',
          }}
        />
      </View>
      <View>
        <Text style={styling.headText}>Hi, Welcome :</Text>
      </View>
      <View>
        <Text style={styling.form}>{userName}</Text>

        <Text style={styling.form}>{name}</Text>
      </View>
      <View style={styling.buttonContainer}>
        <TouchableOpacity style={styling.button} onPress={removeToken}>
          <Text style={styling.textButton}>LOG OUT</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styling = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.purple,
    alignItems: 'center',
    justifyContent: 'center',
  },

  pic: {
    width: 100,
    height: 100,
    marginBottom: 25,
  },
  headText: {
    color: '#FFC200',
    fontSize: 25,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  form: {
    fontSize: 25,
    color: 'white',
    width: 300,
    height: 40,
    marginVertical: 5,
    borderBottomColor: '#FFC200',
    borderBottomWidth: 3,
    fontWeight: 'bold',
  },
  buttonContainer: {
    marginTop: 50,
  },
  button: {
    marginBottom: 25,
    backgroundColor: '#FFC200',
    borderRadius: 25,
    fontSize: 17,
    width: 120,
    height: 40,
    marginHorizontal: 120,
    marginTop: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textButton: {
    color: 'black',
    fontSize: 17,
    fontWeight: 'bold',
  },
});

export default Profile;

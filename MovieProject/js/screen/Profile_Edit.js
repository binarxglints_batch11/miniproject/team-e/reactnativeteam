import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {API_URL1} from '../constant/general';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
} from 'react-native';
// import styles from '../styles/login';
// import ImagePicker from 'react-native-image-crop-picker';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

// import {getUserDetailAction} from '../action/userAction';

const ProfileEdit = () => {
  const dispatch = useDispatch();
  const userDetail = useSelector(state => state.user.userDetail);
  const [image, setImage] = useState(null);

  const [email, setEmail] = useState('');
  const [nama, setNama] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  // useEffect(() => {
  //   dispatch(getUserDetailAction());
  // }, []);

  const editUser = async () => {
    const token = await AsyncStorage.getItem('accessToken');
    const AuthStr = 'Bearer '.concat(token);

    axios({
      method: 'PUT',
      url: `https://septian.dev/movie-api/users/`,
      headers: {Authorization: AuthStr},

      data: {
        email,
        nama,
        password,
        confirmPassword,
      },
    })
      .then(({data}) => {
        alert('edit profile succes');
      })
      .catch(err => {
        alert(err.message);
      });
  };

  return (
    <View style={styles.container}>
      {/* <Image
        source={require('../img/image4.png')}
        style={styles.profileImage}
      /> */}
      <TouchableOpacity
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          top: 45,
          left: 50,
        }}
        onPress={() => openImagePicker()}>
        <MaterialCommunityIcons
          name="camera-account"
          color="white"
          color="white"
          size={30}
        />
      </TouchableOpacity>

      <View>
        <TextInput
          onChangeText={setEmail}
          value={email}
          style={styles.input}
          placeholder={userDetail.email}
          placeholderTextColor="white"
        />
      </View>
      <View>
        <TextInput
          onChangeText={setNama}
          value={nama}
          style={styles.input}
          placeholder={userDetail.nama}
          placeholderTextColor="white"
        />
      </View>
      <View>
        <TextInput
          onChangeText={setPassword}
          value={password}
          style={styles.input}
          placeholder="Password"
          secureTextEntry
          placeholderTextColor="white"
        />
      </View>
      <View>
        <TextInput
          onChangeText={setConfirmPassword}
          value={confirmPassword}
          style={styles.input}
          placeholder="Confirm Password"
          placeholderTextColor="white"
          secureTextEntry
        />
      </View>

      <View style={styles.buttonContainer}>
        <TouchableOpacity
          onPress={() => {
            editUser();
          }}>
          <Text
            style={{
              color: 'black',
              backgroundColor: 'white',
              width: 100,
              textAlign: 'center',
            }}>
            Edit Profile
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ProfileEdit;

//kak irwan
import React, {useState, useEffect} from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
} from 'react-native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const Login = ({navigation}) => {
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');

  const loginAccess = () => {
    axios({
      method: 'POST',
      url: 'https://septian.dev/movie-api/users/login',

      headers: {
        'Content-Type': 'application/json',
      },
      data: {
        userName,
        password,
      },
    })
      .then(({data}) => {
        console.log('ini data', data);
        AsyncStorage.setItem('access_token', data.token, err => {
          console.log('ini token', data.token);
          if (!err) {
            AsyncStorage.setItem('userId', data.id, err2 => {
              if (!err2) {
                setUserName('');
                setPassword('');
                navigation.navigate('MainTab');
              } else {
                alert(err2.message);
              }
            });
          } else {
            alert(err.message);
          }
        });
      })
      .catch(err => {
        setUserName('');
        setPassword('');
        alert(
          'Username or password entered is invalid. Please try again.',
          err.message,
        );
      });
  };

  return (
    <View View style={styles.container}>
      <Image
        style={styles.Logo}
        source={{
          uri: 'https://i.ibb.co/kH410r0/image-1.png',
        }}
      />
      <Text style={styles.pictText}>MilanTV</Text>
      <View style={styles.subContainer}>
        <TextInput
          placeholder="User Name"
          placeholderTextColor="white"
          onChangeText={setUserName}
          value={userName}
          style={styles.input}
        />

        <TextInput
          placeholder="Password"
          placeholderTextColor="white"
          secureTextEntry={true}
          onChangeText={setPassword}
          value={password}
          style={styles.input}
        />
      </View>

      <View style={styles.buttonContainer}>
        <TouchableOpacity onPress={loginAccess}>
          <Text style={styles.btnContainerText}>Login</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.bottomContainer}>
        <Text style={styles.askNew}>
          Don't have an account?{' '}
          <Text
            style={{color: '#FFC200', fontWeight: 'bold'}}
            onPress={() => navigation.navigate('Register')}>
            Register Now !
          </Text>
        </Text>
      </View>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: '#a86462',
    padding: 25,
  },
  Logo: {
    justifyContent: 'center',
    width: 100,
    height: 100,
    marginLeft: 130,
    marginTop: 56,
  },
  pictText: {
    fontSize: 20,
    color: '#F2F2F2',
    marginBottom: 10,
    textAlign: 'center',
  },
  input: {
    width: 320,
    marginTop: 15,
    borderRadius: 15,
    height: 50,
    fontSize: 17,
    marginHorizontal: 15,
    borderBottomWidth: 1,
    borderColor: '#F2F2F2',
    color: 'white',
  },
  buttonContainer: {
    marginBottom: 25,
    backgroundColor: '#FFC200',
    borderRadius: 25,
    fontSize: 17,
    width: 120,
    height: 40,
    marginHorizontal: 120,
    marginTop: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnContainerText: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
  underBtnContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: 10,
  },
  underBtnText: {
    color: 'white',
    fontSize: 14,
    textAlign: 'right',
    width: '100%',
  },
  bottomContainer: {
    marginTop: 30,
    justifyContent: 'flex-start',
  },
  askNew: {
    color: 'white',
    fontSize: 17,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

const Register = ({navigation}) => {
  const [userName, setUserName] = useState('');
  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const RegisterAccess = () => {
    axios({
      method: 'POST',
      url: 'https://septian.dev/movie-api/users/signup',

      headers: {
        'Content-Type': 'application/json',
      },
      data: {
        userName,
        email,
        name,
        password,
        confirmPassword,
      },
    })
      .then(({data}) => {
        console.log('inidata', data);
        AsyncStorage.setItem('userId', data.id, err => {
          if (!err) {
            setUserName('');
            setEmail('');
            setName('');
            setPassword('');
            setConfirmPassword('');
            navigation.navigate('Login');
          } else {
            alert(err.message);
          }
        });
      })
      .catch(err => {
        setUserName('');
        setEmail('');
        setName('');
        setPassword('');
        setConfirmPassword('');
        alert('You already have an account', err.message);
      });
  };

  return (
    <View style={styles.container}>
      <Image
        style={styles.Logo}
        source={{
          uri: 'https://i.ibb.co/d7TjMCs/image-3.png',
        }}
      />
      <Text style={styles.pictText}>Register</Text>
      <View style={styles.subContainer}>
        <TextInput
          placeholder="UserName"
          placeholderTextColor="white"
          onChangeText={userName => setUserName(userName)}
          style={styles.input}
          value={userName}
        />
        <TextInput
          placeholder="Email"
          placeholderTextColor="white"
          style={styles.input}
          onChangeText={email => setEmail(email)}
          value={email}
        />
        <TextInput
          placeholder="Name"
          placeholderTextColor="white"
          onChangeText={name => setName(name)}
          value={name}
          style={styles.input}
        />

        <TextInput
          placeholder="Password"
          placeholderTextColor="white"
          secureTextEntry={true}
          onChangeText={password => setPassword(password)}
          value={password}
          secureTextEntry={true}
          style={styles.input}
        />
        <TextInput
          placeholder="Confirm Password"
          placeholderTextColor="white"
          onChangeText={confirmPassword => setConfirmPassword(confirmPassword)}
          style={styles.input}
          secureTextEntry={true}
          value={confirmPassword}
        />
      </View>
      <View>
        <TouchableOpacity
          onPress={RegisterAccess}
          style={styles.buttonContainer}>
          {/* <TouchableOpacity style={styling.button} onPress={() => handleRegister()}></TouchableOpacity> */}
          <Text style={styles.btnContainerText}>Register</Text>
        </TouchableOpacity>
        <View style={styles.bottomContainer}>
          <Text style={styles.askNew}>
            Already have an account?{' '}
            <Text
              style={{color: '#FFC200', fontWeight: 'bold'}}
              onPress={() => navigation.navigate('Login')}>
              Login !
            </Text>
          </Text>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: '#a86462',
    padding: 15,
  },
  input: {
    width: 320,
    marginTop: 5,
    borderRadius: 15,
    height: 50,
    fontSize: 17,
    marginLeft: 15,
    paddingHorizontal: 15,
    borderBottomWidth: 1,
    borderColor: '#F2F2F2',
    color: 'white',
  },
  pictText: {
    fontSize: 20,
    color: '#F2F2F2',
    marginBottom: 10,
    textAlign: 'center',
  },
  Logo: {
    justifyContent: 'center',
    width: 100,
    height: 100,
    marginLeft: 125,
    marginTop: 20,
  },
  buttonContainer: {
    marginBottom: 25,
    backgroundColor: '#FFC200',
    borderRadius: 25,
    fontSize: 17,
    width: 120,
    height: 40,
    marginHorizontal: 120,
    marginTop: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnContainerText: {
    color: 'white',
    fontSize: 17,
    alignItems: 'center',
  },
  underBtnContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: 10,
  },
  underBtnText: {
    color: '#F2F2F2',
    fontSize: 14,
    alignItems: 'center',
  },
  bottomContainer: {
    marginTop: 30,
    justifyContent: 'flex-start',
  },
  askNew: {
    color: 'white',
    fontSize: 17,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  Register: {
    fontSize: 20,
    color: '#F2F2F2',
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
  },
});
export default Register;

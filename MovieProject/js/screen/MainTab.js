import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {color} from '../styles/default';
// Screen
import Home from './Home';
import Profile from './Profile';

const Tab = createBottomTabNavigator();
const ProfileStack = createDrawerNavigator();
const HomeStack = createStackNavigator();

const MainTab = ({navigation}) => (
  <Tab.Navigator
    initialRouteName="Home"
    activeColor="white"
    inactiveColor="black"
    barStyle={{
      backgroundColor: 'gray',
      paddingVertical: 5,
    }}>
    <Tab.Screen
      name="Home"
      component={Home}
      options={{
        tabBarLabel: 'Home',
        tabBarColor: color.purple,
        tabBarIcon: ({color}) => (
          <MaterialIcons name="home-filled" color={color} size={25} />
        ),
      }}
    />

    <Tab.Screen
      name="Profile"
      component={ProfileStackScreen}
      options={{
        tabBarLabel: 'Profile',
        tabBarColor: color.purple,
        tabBarIcon: ({color}) => (
          <MaterialIcons name="account-circle" color={color} size={25} />
        ),
      }}
    />
  </Tab.Navigator>
);

const ProfileStackScreen = ({navigation}) => (
  <ProfileStack.Navigator
    drawerPosition="right"
    screenOptions={{
      headerStyle: {
        backgroundColor: 'white',
      },
      headerTintColor: 'black',
      fontWeight: 'bold',
    }}>
    <ProfileStack.Screen
      name="Profile"
      component={Profile}
      options={{
        title: 'Edit Profile',
        headerRight: () => (
          <MaterialIcons
            name="list"
            color={'black'}
            size={25}
            onPress={() => navigation.navigate('DrawerContent')}
          />
        ),
      }}></ProfileStack.Screen>
  </ProfileStack.Navigator>
);
export default MainTab;

import React, {useEffect} from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

function SplashScreen({navigation}) {
  useEffect(() => {
    setTimeout(handleToken, 3000);
  }, []);

  const handleToken = () => {
    AsyncStorage.getItem('access_token', (err, res) => {
      if (res) {
        AsyncStorage.getItem('userId', (err2, res2) => {
          if (res2) navigation.navigate('MainTab');
          else navigation.navigate('Login');
        });
      } else {
        navigation.navigate('Login');
      }
    });
  };
  return (
    <View style={styles.container}>
      <Image
        style={styles.Logo}
        source={{
          uri: 'https://i.ibb.co/kH410r0/image-1.png',
        }}
      />
      <Text style={styles.picText}>MilanTV</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#a86462',
    alignItems: 'center',
    justifyContent: 'center',
  },
  Logo: {
    width: 125,
    height: 125,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  picText: {
    marginTop: 20,
    color: '#FFC200',
    fontSize: 35,
  },
});

export default SplashScreen;
